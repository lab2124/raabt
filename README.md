# Reproducible Authorship Attribution Benchmark Tasks (RAABT)

Version 1.0

RAABT consists of five closed-set authorship identification experiments.

## Task Descriptions

Training set and test set documents for each task live inside subdirectories of `data`. For example, training set documents for the AAAC-fixed-topic task are found in `data/AAAC-fixed-topic/train`.

*Note: All texts have been converted to UTF-8 format.*

1. **AAAC-fixed-topic**: Ad-hoc Authorship Attribution Competition, fixed topic. The first task is "Problem A" from the 2004 Ad-hoc Authorship Attribution Competition (AAAC) (Juola 2006). Texts were gathered from 13 authors in a 2013 undergraduate writing course at an American university. For the test set documents, participants were asked to write on the topic of "work."
2. **AAAC-free-topic**: Ad-hoc Authorship Attribution Competition, free topic. The second task is "Problem B" from the AAAC. Test documents are additional course essays on other topics. Test set documents do not overlap with training documents. Training documents are the same as in the first task.
3. **EBG-obfuscation**: Extended Brennan-Greenstadt Corpus, obfuscation condition. The Extended Brennan-Greenstadt Corpus (Brennan, Afroz, and Greenstadt 2012) contains writing from 45 individuals contacted through the Amazon Mechanical Turk platform no later than the year 2012. Participants uploaded examples of their writing. The researchers asked for writing of a "scholarly" nature.
    Participants were then asked to write a short essay on a fixed topic. They were asked to describe their neighborhood to someone unfamiliar with the location. Notably, they were also asked to obscure their writing style. They were, however, not given any instructions on how to accomplish this. These essays form the test set.
    Given prevailing norms on Amazon Mechanical Turk and the monetary incentive to finish the task quickly (participants' payment did not depend on time spent on the task) we suspect many participants did not devote considerable time to devising strategies for obscuring their writing style. We suggest that this task be treated as, in essence, an additional fixed topic task.
4. **RJ-fixed-topic** Riddell-Juola Corpus, control condition. The Riddell-Juola Corpus collects texts using essentially the same techniques were used in Brennan, Afroz, and Greenstadt (2012). Responses were collected in March and June of 2019. According to self-reported gender and age, participant demographic characteristics are roughly balanced. Participants were asked to respond to the same "describe your neighborhood" prompt mentioned earlier. No further instructions were given.
5. **RJ-obfuscation** Riddell-Juola Corpus, obfuscation condition. This task is the same as RJ-fixed-topic with one difference. Participants were told to obscure their writing using the same instruction as found in EBG-obfuscation. Again, they were given no instructions on how to accomplish this task.  Participants were randomly assigned to receive the obfuscation instruction. Therefore the authors of the test set documents in this task do not overlap with the authors of the test set documents in RJ-fixed-topic.  The training sets for the two tasks involving the Riddell-Juola Corpus are the same.

## Sources

Apart from UTF-8 conversion, files should be identical to the originals.

- [Ad-hoc Authorship Attribution Competition](http://www.mathcs.duq.edu/~juola/authorship_materials2.html)
- [Extended Brennan-Greenstadt Corpus](https://github.com/psal/jstylo/tree/master/jsan_resources/corpora/amt)
- [Riddel-Juola Corpus](https://github.com/literary-materials/defending-against-authorship-attribution-corpus/)

## Non-trivial baseline

Running ``python3 scripts/calculate-accuracy.py`` will perform a simple authorship attribution analysis and print test set accuracy for each task. Function word frequencies are used as input features.
Running the script requires the following:

- Python 3.7 or higher
- scikit-learn 0.24

The script is concise and should be very easy to read.

## Public domain

Authors waived rights to contributed materials. All other material, such as scripts and documentation, are dedicated to the public domain.

## References

- Michael Brennan, Sadia Afroz, and Rachel Greenstadt. 2012. Adversarial Stylometry: Circumventing Authorship Recognition to Preserve Privacy and Anonymity. *ACM Trans. Inf. Syst. Secur.*, 15(3):12:1–12:22.
- Patrick Juola. 2006. Authorship Attribution. *Foundations and Trends in Information Retrieval*, 1(3):233–334.
- Koppel, M., Schler, J., & Argamon, S. (2009). Computational methods in authorship attribution. *Journal of the American Society for information Science and Technology*, 60(1), 9-26.

## Version History

- 1.0: Initial release.
