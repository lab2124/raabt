#!/usr/bin/env python3
"""
Calculate simple baselines for the Reproducible Authorship Attribution Benchmark Tasks (RAABT)

See the README for more information about this script.

"""
import os
import re

import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import Normalizer, StandardScaler
from sklearn.svm import SVC

KOPPEL512 = open("scripts/koppel_function_words.txt", "r").read().splitlines()
TASKS = ["AAAC-fixed-topic", "AAAC-free-topic", "EBG-obfuscation", "RJ-fixed-topic", "RJ-obfuscation"]


def get_feature_matrix(raw):
    return np.array([len(re.findall(r"\b" + fw + r"\b", raw.lower())) for fw in KOPPEL512])


for task in TASKS:
    X, y = {}, {}
    # extract features and labels
    for split_ in ["train", "test"]:
        if task.startswith("AAAC"):
            X[split_], y[split_] = zip(*[
                (get_feature_matrix(open(f).read()), f.name.split("-" if split_ == "train" else ".")[0][-2:])
                for f in os.scandir(os.path.join("data", task, split_))
            ])
        elif task.startswith("EBG"):
            X[split_], y[split_] = zip(*[
                (get_feature_matrix(open(f).read()), f.name.split("_")[0])
                for f in os.scandir(os.path.join("data", task, split_))
            ])
        else:
            X[split_], y[split_] = zip(*[
                (get_feature_matrix(open(f).read()), f.name.split("_" if split_ == "train" else ".")[0])
                for f in os.scandir(os.path.join("data", task, split_))
            ])

    # normalize, standardize, fit training samples, then score on testing examples
    print(f"{task} test set accuracy:")
    print(f"• Random chance: {1 / len(set(y['train'])):.1%}")
    for learner in [
        ("Linear SVM", SVC(C=1, kernel="linear", max_iter=-1)),
        ("Logistic regression", LogisticRegression(C=1, solver="lbfgs", max_iter=1e9)),
    ]:
        pipeline = Pipeline([("normalizer", Normalizer(norm="l1")), ("scaler", StandardScaler()), learner])
        pipeline.fit(X["train"], y["train"])
        # uncomment the following three lines if accuracies of LOO-CV on training examples are desired
        # from sklearn.model_selection import LeaveOneOut, cross_val_score
        # loocv = cross_val_score(pipeline, X['train'], y['train'], cv=LeaveOneOut())
        # print(f"• {learner[0]}: LOO-CV accuracy (training set only) is {loocv.mean():.1%}")
        print(f"• {learner[0]}: {pipeline.score(X['test'], y['test']):.1%}")
